from flask import *
import sqlite3

app = Flask(__name__)
names = ['Zorro']

@app.route('/')
def hello():
	return 'Hello <ul>' + ''.join(['<li> ' + n for n in names]) + '</ul>\n', 200

@app.route('/user/<uname>', methods=['PUT'])
def add (uname):
	names.append(uname)
	connexion = sqlite3.connect("flasktd.db")
	cur = connexion.cursor()
	sql = "INSERT INTO utilisateurs (nom) VALUES (?)"
	cur.execute(sql,[uname])
	connexion.commit()
	return 'User ' + uname + ' added.\n', 201

@app.route('/user/<uname>', methods=['DELETE'])
def rem (uname):
	if uname not in names:
		return 'User ' + uname + ' does not exists.\n', 404
	names.remove(uname)
	connexion = sqlite3.connect("flasktd.db")
	cur = connexion.cursor()
	sql = "DELETE FROM utilisateurs WHERE nom = ?"
	cur.execute(sql,[uname])
	connexion.commit()
	return 'User ' + uname + 'removed.\n', 200

app.run(host='0.0.0.0', debug=True)
#commande curl pour ajout d'un user 
#curl -X PUT "http://localhost:5000/user/edouard"

#pour la supression
#curl -X DELETE "http://localhost:5000/user/edouard"

