# initialization vector 32 Bytes
iv = "01111000011000010111001101101000"

def str_to_bin(a_str):
	# Converting String to binary
	'''
		convert a string of characters s to a string of 8 bits per character
	'''
	b = ""
	for c in a_str:
		b += "{:08b}".format(ord(c))
	return b

# to Binary number  
def dec_to_bin(n):
	return bin(n).replace("0b", "")  
    
# function who take two binary number in parameters and who does an XOR
def comp(a,b):
	c = "0b"+str(a)
	d = "0b"+str(b)
	a_bin = bin(int(c,2) ^ int(d,2))
	return a_bin

# function who does padding with 0
def padding_xd(a):
	taille_a = len(a)
	for i in range(taille_a, 32):
		a = str(a)+"0"
	return a

# function who returns the hash code with the xd method
# he takes a string (like a word) in parameter and
# depending on the size he cuts this word into a group of 32 bytes
# for respect merkle-damgard construction
def hash_xd(c):
	c_bin = str_to_bin(c)
	taille_iv = len(iv)
	taille_c_bin = len(c_bin)

	if taille_iv >= taille_c_bin:
		a = padding_xd(c_bin)
		y = comp(a,iv)
	else:
		nbr_bloc = int(taille_c_bin / 32)
		nbr_bit = nbr_bloc * 32
		f = iv
		j = 1
		for i in range(0, nbr_bit, 32):
			res = comp(str(c_bin)[i:j*32],f)
			f = str(res)[2:]
			j = j + 1

		a = padding_xd(str(c_bin)[nbr_bit:])
		y = comp(a,f)
		

	in_hex = hex(int(y, 2))
	return str(in_hex)[2:10]

def padding_xdd(a):
	taille_a = len(a)
	a = str(a) + "1"
	for i in range(taille_a+1, 32):
		a = str(a)+"0"
	return a

def hash_xdd(c):
	c_bin = str_to_bin(c)
	taille_iv = len(iv)
	taille_c_bin = len(c_bin)

	if taille_iv >= taille_c_bin:
		a = padding_xdd(c_bin)
		y = comp(a,iv)
	else:
		nbr_bloc = int(taille_c_bin / 32)
		nbr_bit = nbr_bloc * 32
		f = iv
		j = 1
		for i in range(0, nbr_bit, 32):
			res = comp(str(c_bin)[i:j*32],f)
			f = str(res)[2:]
			j = j + 1

		a = padding_xdd(str(c_bin)[nbr_bit:])
		y = comp(a,f)
		

	in_hex = hex(int(y, 2))
	return str(in_hex)[2:10]


def padding_xddd(a):
	taille_a = len(a)
	taille_a_bin = dec_to_bin(taille_a)
	a = str(a) + "1"
	for i in range(taille_a+1, 32-len(taille_a_bin)):
		a = str(a)+"0"
	a  = str(a)+str(taille_a_bin)
	return a

def hash_xddd(c):
	c_bin = str_to_bin(c)
	taille_iv = len(iv)
	taille_c_bin = len(c_bin)

	if taille_iv >= taille_c_bin:
		a = padding_xddd(c_bin)
		y = comp(a,iv)
	else:
		nbr_bloc = int(taille_c_bin / 32)
		nbr_bit = nbr_bloc * 32
		f = iv
		j = 1
		for i in range(0, nbr_bit, 32):
			res = comp(str(c_bin)[i:j*32],f)
			f = str(res)[2:]
			j = j + 1

		a = padding_xddd(str(c_bin)[nbr_bit:])
		y = comp(a,f)
		

	in_hex = hex(int(y, 2))
	return str(in_hex)[2:10]

# initializing string
c = "CHAINE VIDE"

# call xd function
y = hash_xd(c)

# printing result 
print(y)


'''
Collision : deux mot qui on le même hash

attaque de préimage : on nous donne le hash et il nous retourne le x (l'input)

attaque de second préimage : A partir du hash de x, on essaie de trouver un y qui diffère de x mais dont le hash de y = x

'''